<?php

use App\Http\Controllers\listUserController;
use App\Http\Controllers\SocialLoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',[SocialLoginController::class,'auth']);
Route::middleware('basic_auth')->group(function(){
    Route::get('/user',[SocialLoginController::class,'viewUser']);
    Route::delete('/logout',[SocialLoginController::class,'logout']);
});
Route::group(['prefix'=>'/social-login/{provider}'],function(){
    Route::post('/',[SocialLoginController::class,'redir']);
    Route::get('/callback',[SocialLoginController::class,'callback']);
});
Route::prefix('/listConnectedUser')->group(function(){
    Route::get('/',[listUserController::class,'index']);
});