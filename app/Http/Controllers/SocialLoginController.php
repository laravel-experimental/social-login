<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use Laravel\Socialite\Facades\Socialite;

class SocialLoginController extends Controller
{
    public function redir($provider){
        Log::info('Social Login Redirect',['provider'=>$provider]);
        
        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function callback(Request $request, $provider){
        if($request->input('error_code')!=null){
            return redirect('/')->with(['errorMsg'=>$request->input('error_message')]);
        }
        $userData=Socialite::driver($provider)->stateless()->user();
        Log::info('Social Login Callback',['provider'=>$provider,'data'=>json_encode($userData)]);

        $existingData=DB::table('data_users')
        ->select('data')
        ->addSelect(DB::raw('hex(view_id) as uid'))
        ->where('username','=',$userData->id)->first();
        if($existingData==null){
            DB::table('data_users')->insert([
                'username'=>$userData->id,
                'data'=>json_encode($userData,JSON_UNESCAPED_SLASHES),
                'login_platform'=>1
            ]);
        }
        else{
            DB::table('data_users')
            ->where(['username'=>$userData->id])
            ->update([
                'updated_at'=>date('Y-m-d H:i:s'),
                'data'=>json_encode($userData,JSON_UNESCAPED_SLASHES),
                'login_platform'=>1
            ]);
        }
        
            $existingData=DB::table('data_users')
            ->select('data')
            ->addSelect(DB::raw('hex(view_id) as uid'))
            ->where('username','=',$userData->id)->first();
        if($existingData!=null){
            session([
                'is_login'=>true,
                'uid'=>$existingData->uid,
                'udata'=>$existingData
            ]);
        }
        return redirect(url('/user'));
    }

    public function viewUser(){
        $uid=session('uid',null);
        $userData=DB::table('data_users')
        ->select('data')
        ->addSelect(DB::raw('hex(view_id) as uid'))
        ->where(DB::raw('hex(view_id)'),'=',$uid)->first();
        Log::info('Social Login User',['userdata'=>json_encode($userData)]);
        if($userData!=null){
            return view('user',compact('userData'));
        }
        else{
            return redirect(url('/'));
        }
    }

    public function auth(){
        if(session('is_login',false)){
            return redirect('/user');
        }
        else{
            return view('auth');
        }
    }

    public function logout(){
        $accessToken=json_decode(session('udata')->data)->token;
        Http::get('https://graph.facebook.com/v2.9/me/permissions',['access_token'=>$accessToken]);
        Session::flush();
        return redirect('/');
    }
}
