<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class listUserController extends Controller
{
    //
    public function index(){
        $AUTH_USER = 'us3rLiSt123';
        $AUTH_PASS = 'us3rLiStPas!@#';
        header('Cache-Control: no-cache, must-revalidate, max-age=0');
        $has_supplied_credentials = !(empty($_SERVER['PHP_AUTH_USER']) && empty($_SERVER['PHP_AUTH_PW']));
        $is_not_authenticated = (
            !$has_supplied_credentials ||
            $_SERVER['PHP_AUTH_USER'] != $AUTH_USER ||
            $_SERVER['PHP_AUTH_PW']   != $AUTH_PASS
        );
        if ($is_not_authenticated) {
            header('HTTP/1.1 401 Authorization Required');
            header('WWW-Authenticate: Basic realm="Access denied"');
            return response()->json(['error'=>'Unauthorized'],401);
        }
        
            $data=DB::table('data_users')
            ->select(DB::raw("replace(json_extract(data, '$.name'),'\"','') as name "))
            ->addSelect(DB::raw("replace(json_extract(data, '$.email'),'\"','') as email "))
            ->addSelect(DB::raw("replace(json_extract(data, '$.avatar'),'\"','') as avatar "))
            ->get();
            return response()->json(['connectedUser'=>$data],200);
    }
}
