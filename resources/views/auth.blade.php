@extends('default')
@section('title')
    <title>LOGIN</title>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-4"></div>    
        <div class="card col-md-4">
            @if(session('errorMsg')!=null)
            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                <strong>An error occured!</strong>
                {{session('errorMsg')}}
                <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
            </div>
            @endisset
            <div class="card-header">
                <h5>Login with Social Account</h5>
            </div>
            <div class="card-body">
                <center>
                <form action="{{url('social-login/facebook')}}" method="POST">
                    @csrf
                    <button class="btn btn-block btn-primary"><i class="fab fa-facebook"></i> Login with Facebook</button>
                </form>
                </center>
            </div>
        </div>
        <div class="col-md-4"></div>
    </div>
@endsection