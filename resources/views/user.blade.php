@extends('default')
@section('title')
    <title>USER DATA</title>
@endsection
@section('content')
    @php
        $uData=json_decode($userData->data);
    @endphp
    <div class="row">
        <div class="col-md-4">
        </div>
        <div class="card col-md-4">
            <div class="card-body">
                <img style="border-radius:50%; height: 100px;width:100px;object-fit:contain;" src="{{$uData->avatar}}" alt="">
                <br>
                <b>Name : </b> <span>{{$uData->name}}</span>
                <br>
                <b>Email : </b> <span>{{$uData->email}}</span>
                <hr>
                <form action="/logout" method="POST">
                    @method('delete')
                    @csrf
                    <button class="btn btn-block btn-danger"><i class="fa fa-lock"></i> Logout</button>
                </form>
            </div>
        </div>
        <div class="col-md-4">
        </div>
    </div>
@endsection