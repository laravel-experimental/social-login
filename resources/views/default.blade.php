<html>
    <head>
        @hasSection ('title')
            @yield('title')
        @else
            <title>PENTEST FB LOGIN</title>
        @endif
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css">
    </head>
    <body>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha256-4+XzXVhsDmqanXGHaHvgh1gMQKX40OUvDEBTu8JcmNs=" crossorigin="anonymous"></script>
        <div class="container" style="padding: 16px">
            <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">!EXPERIMENTAL!</h4>
            <p>This tool is for <b>Testing only</b>.Do login with your associated social account within this tool is mean you <b>have been agreed</b> with:</p>
            <ul>
                <li>Any data recorded for logging</li>
                <li>Owner is not responsible of your data security or any demage happend with your data in the future</li>
                <li>You already realize if you do with your own risk</li>
            </ul>
            </div>
            @yield('content')
        </div>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/js/all.min.js"></script>
    </body>
</html>